import request from '@/utils/request'
import qs from 'qs'

export function dashboard(data) {
  return request({
    url: 'home/dashboard?' + qs.stringify(data),
    method: 'get'
  })
}
