// import { Toast } from 'mint-ui';
export default {
  // 获取网址的参数
  getUrlParms(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')
    const path = window.location.search.substr(1)
    const hash = window.location.hash
    const index = hash.lastIndexOf('?')
    let str
    if (path) {
      str = path
    } else {
      str = hash.substring(index + 1, hash.length)
    }
    const r = str.match(reg)
    // let reg_chinese = new RegExp('[\\u4E00-\\u9FFF]+', 'g')  // 检测中文
    if (r !== null) {
      return decodeURI(r[2])
      // unescape(r[2]) encodeURI()把中文转义
    }
    return null
  },
  // HTML转义
  HTMLEncode(html) {
    var temp = document.createElement('div');
    (temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html)
    var output = temp.innerHTML
    temp = null
    return output
  },
  // HTML 反转义
  HTMLDecode(text) {
    var temp = document.createElement('div')
    temp.innerHTML = text
    var output = temp.innerText || temp.textContent
    temp = null
    return output
  },
  getLocal(key) {
    var val = window.localStorage.getItem(key)
    if (val === 'undefined') {
      return ''
    }
    return window.localStorage.getItem(key)
  },
  setLocal(key, val) {
    window.localStorage.setItem(key, val)
    return true
  },
  getSession(key) {
    var val = window.sessionStorage.getItem(key)
    if (val === 'undefined') {
      return ''
    }
    return window.sessionStorage.getItem(key)
  },
  setSession(key, val) {
    window.sessionStorage.setItem(key, val)
    return true
  }
}

