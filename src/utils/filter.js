import Vue from 'vue'

Vue.filter('filterTime', (time, formater, ...args) => {
  if (!time) return time

  // let time = timeStamp.toString().replace("/Date(", "").replace(")/", "")
  const date = new Date(time.replace(/-/g, '/')) // parseInt(time)

  let fmt = (formater != null) ? formater : 'yyyy-MM-dd hh:mm:ss'
  const o = {
    'M+': date.getMonth() + 1, // 月
    'd+': date.getDate(), // 日
    'h+': date.getHours(), // 小时
    'm+': date.getMinutes(), // 分
    's+': date.getSeconds(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds() // 毫秒
  }

  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  for (const k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) { fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length))) }
  }

  return fmt
})

Vue.filter('timestampToTime', (timestamp, ...args) => {
  var date = new Date(timestamp)// 时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + '-'
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-'
  var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' '
  var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':'
  var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())
  // var s = date.getSeconds()
  return Y + M + D + h + m
})

Vue.filter('intervalTime', (time, ...args) => {
  var stime = Date.parse(new Date(time))
  var etime = (new Date()).valueOf()
  var usedTime = etime - stime // 两个时间戳相差的毫秒数
  // var days = Math.floor(usedTime / (24 * 3600 * 1000))
  // 计算出小时数
  var leave1 = usedTime % (24 * 3600 * 1000) // 计算天数后剩余的毫秒数
  var hours = Math.floor(leave1 / (3600 * 1000))
  // 计算相差分钟数
  var leave2 = leave1 % (3600 * 1000) // 计算小时数后剩余的毫秒数
  var minutes = Math.floor(leave2 / (60 * 1000))
  return hours + '小时' + minutes + '分钟'
})

Vue.filter('filterNumber', (num, formater, type, ...args) => {
  const arr_txt = ['千', '万', '百万', '千万', '亿万']
  const arr_word = ['K', 'w', 'm', 'kw', '亿万']
  // type = type || 'word'
  const arr = type === 'word' || !type ? arr_word : arr_txt
  let newnum = Number(num) // parseInt parseFloat
  if (newnum < 1000) {
    return newnum
  }
  if (newnum >= 1000 && newnum < 10000) {
    newnum = parseFloat((newnum / 1000).toFixed(2)) + arr[0]
  } else if (newnum >= 10000) {
    newnum = parseFloat((newnum / 10000).toFixed(2)) + arr[1]
  }
  return newnum
})

// 过滤没有 http 的图片地址
Vue.filter('filterImgUrlH', (url, formater, ...args) => {
  const reg = /(http|https):\/\/([\w.]+\/?)\S*/
  reg.test(url) || (url = 'http://' + url)
  return url
})

