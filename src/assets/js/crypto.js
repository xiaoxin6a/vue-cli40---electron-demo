import CryptoJS from 'crypto-js'
// DES加密解密
const key = CryptoJS.enc.Utf8.parse("i3nbasb2u15abo91238basdb");
const iv = CryptoJS.enc.Utf8.parse('i3nbasb2u15abo91238basdb');

export default {
 encryptByDES (message){
    const encrypted = CryptoJS.TripleDES.encrypt(message, key, {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    })
    let oldHexStr = CryptoJS.enc.Hex.parse(encrypted.ciphertext.toString())
    let base64Str = CryptoJS.enc.Base64.stringify(oldHexStr);
    return base64Str
  },
  decryptByDES (ciphertext) {
    const decrypted = CryptoJS.TripleDES.decrypt({
      ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
    }, key, {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    })
    return decrypted.toString(CryptoJS.enc.Utf8)
  }
}